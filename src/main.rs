use mastodon_async::{
    prelude::*,
    Result,
    helpers::env::from_env as data_from_env,
};

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()> {
    dotenvy::dotenv().unwrap();
    let data = data_from_env()?;

    let mastodon = Mastodon::from(data);

    let status = StatusBuilder::new()
        .status("Some status\nWoooo, another line!")
        .visibility(Visibility::Unlisted)
        .build()?;

    mastodon.new_status(status).await?;

    Ok(())
}
