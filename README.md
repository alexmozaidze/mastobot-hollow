# MastoBot Hollow

A useless bot with no purpose. All it does is post:

> Some status  
> Woooo, another line!

# Why?

Because I didn't use Rust in a while and wanted to refresh my memory.

One day, I'll define a purpose to this bot, but until then:

> Some status  
> Woooo, another line!
